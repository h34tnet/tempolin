# Tempolin

## What is Tempolin?

Tempolin is a templating engine that takes specially formatted text templates and emits kotlin code which can be used to
create text output using dynamic data during runtime. It's evoked as a gradle build task.

## Features

* Performance: Tempolin is _very_ fast, as all parsing is done during project compile time. The emitted code is then
  being JIT-compiled together with the application like any ordinary kotlin code. It's roughly twice as fast as
  mustache.java.
* No file IO during runtime (i.e. no blocking, IO is done only during the compilation step - except for the JVMs startup
  itself).
* IDE auto-completion support for all IDEs without any extra plug-ins necessary.
* A certain amount of (type) safety: placeholders _must_ be assigned in the template constructor.
* Bytecode obfuscators work on the templates.
* Blocks (repeated elements) are assigned as `Sequence` which support lazy evaluation. Constant memory consumption
  is possible when writing to a stream.

## Limitations

* After editing templates the project must be recompiled (dynamic class loading may be possible but is currently
  untested).

## Supported markup

* `{$placeholder}`: creates a placeholder for a string value.
* `{$placeholder|modifier1|modifier2}`: setter that also applies the given modifiers to the value. Modifiers are static
  String->String functions defined in a special class.
* `{*$placeholder}`: creates a placeholder for a string value with a "starPrefix" Modifier added. This is usable for
  common escaping or encoding actions, e.g. html-entity encoding. Can be combined with other modifiers and is executed
  last. There are several possible prefixes: * = starPrefix, # = hashPrefix, + = plusPrefix, ~ = tildePrefix
* `{*%myFunction:arg|modifier1}`: calls a function with this name from the `Modifier`, passing the "arg" as a
  string and applies the "starPrefix"-modifier and the "modifier1" function to the output.
* `{for $block}...{/for}`: creates a subclass from the content that can be assigned 0-n times.
* `{template $mytemplate}`: embeds a different template here.
* `{if $condition}...{/if}`: creates a conditional. currently, conditionals only check for empty strings or lists,
  depending on the type of $condition, or they're booleans, if not used anywhere else.
* `{if $condition}...{else}...{/if}`: creates a conditional with an alternative.
* `{literal}...{/literal}` to exclude whole regions from parsing. Everything inside literal sections is emitted as-is.
  Literal sections can't be nested.
* `{comment}...{/comment}` to exclude whole regions from processing - nothing is emitted. Comment sections can't be
  nested.

## Example

A template `Example.html` with the content

```html 
<h1>{*$headline}</h1>
<p>Welcome to {*%constants:name}</p>
{if $showTeaser}
<p class="teaser">{*$teaser|firstLine}</p>
{/if}
<ul>
    {for $bullets}
    <li>{*$text}</li>
    {/for}
</ul>
{*%constants:footer}
```

generates a kotlin class `Example.kt` that can be used as follows:

```kotlin
val modifiers = object : Example.Modifiers {
    override fun constants(arg: String) = when (arg) {
        "name" -> "Tempolin Example"
        "footer" -> "good bye!"
        else -> throw Exception("unknown constant $arg")
    }

    override fun starPrefix(str: String) = Html.escape(str)

    override fun firstLine(str: String) = str.lines().firstOrNull() ?: ""
}

val numbers = listOf("one", "<two>", "three")

Example(
    modifiers = modifiers,
    headline = "Hello Tempolin",
    showTeaser = true,
    teaser = "hello & world\nfrom Tempolin",
    bullets = { numbers.map { BulletsBlock(text = it) }.asSequence() }
).let { print(it.toString()) }
```

prints the following:

```html
<h1>Hello Tempolin</h1>
<p>Welcome to Tempolin Example</p>
<p class="teaser">hello &amp; world</p>
<ul>
    <li>one</li>
    <li>&lt;two&gt;</li>
    <li>three</li>
</ul>
good bye!
```

The template expects an instance of the Modifiers interface, which contains method definitions for all the modifiers and
functions used. Since all your templates probably have mostly the same modifiers, it's possible to have one Modifiers
class implementing all of your template Modifier interfaces.

## Usage

### Stream writing

Template output is written continuously, including blocks in a loop. This means that sequences like from
`File.useLines { sequence -> ... }` can be read and written one-by-one with constant memory consumption when using
writeTo(`appendable`).

Example: This creates an HTML list of all comments the `bigInputFile.txt` together with the line number.

```html
MyTemplate.tpl.html
<ol>
    {for $lines}
    <li>{$num}: {$line}</li>
    {/for}
</ol>
```

```kotlin
// this reads one line from bigInputFile.txt, applies the templating and writes it to formattedOutput.txt 
File("formattedOutput.txt").bufferedWriter().use { appendable ->
    File("bigInputFile.txt").useLines { lineSeq ->
        MyTemplate(
            lines = {
                lineSeq.mapNotNullIndexed { idx, line ->
                    if (line.trimStart().startsWith("//")) LinesBlock(num = idx, line = line) else null
                }
            }
        ).writeTo(appendable)
    }
}
```

### Whitespace handling

Tempolin preserves the original line endings and whitespace. The exception being lines with only `{for $var}`, `{/for}`,
`{if $cond}`, `{else}` or `{/if}` tokens plus extra whitespace; in that case, the extra whitespace is removed.

## How to use

in build.gradle.kts:

```kotlin
plugins {
    id("net.h34t.tempolin") version "1.0.2"
}

// manually add the generated sources; alternatively, set addSources = true in the configuration 
sourceSets {
    main {
        kotlin {
            // include the generated source files 
            srcDir("$buildDir/generated-sources/tempolin")
        }
    }
}

// Optional: run tempolin automatically when building 
// (alternatively, run the task manually after changing templates) 
tasks.getByName("compileKotlin") {
    dependsOn("tempolin")
}

tempolin {
    add {
        // place all templates in this directory
        // subdirectories become packages 
        templateDirectory = "$projectDir/src/main/tpl"

        // output directory for the generated templates
        outputDirectory = "${layout.buildDirectory.get()}/generated-sources/tempolin"

        // currently, there's only the "kotlin" generator
        // optional, default is "kotlin"
        compilationTarget = "kotlin"

        // an optional input file filter
        fileFilter = { file -> file.extension == "html" }

        // set the name and package of the template interface
        // defaults to "net.h34t.TempolinTemplate"
        templateInterface = "my.app.TemplateInterface"

    }

    add {
        templateDirectory = "$projectDir/myTemplates"
        outputDirectory = "${layout.buildDirectory.get()}/generated-sources/myTemplates"

        // Automatically adds this outputDirectory to the sourceSets
        // defaults to false. 
        addSources = true

        fileFilter = { file -> file.name.endsWith(".tpl.html") }

        // Uses the default templateInterface name "net.h34t.TempolinTemplate"
        // but doesn't create the file. Can be used to avoid clashes with 
        // imported code that also uses templates.
        addTemplateInterface = false
    }
}
```

## License

MIT License

Copyright (c) 2024 Stefan Schallerl

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
