package net.h34t.tempolin

import org.junit.jupiter.api.Test
import java.nio.file.Path
import java.nio.file.Paths

class TemplateFileCollectorTest {

    @Test
    fun `test file collection`() {
        val tfc = TemplateFileCollector()
        val entries: List<Path> = tfc.findFilesInDirectory(
            Paths.get("src/test/resources/tpl"),
        ) { file -> file.name.endsWith(".tpl.html") }

        assert(entries.size == 4) { "Wrong number of files (${entries.size})." }

        assert(entries.contains(Paths.get("com/mypackage/common/Footer.tpl.html")))
        assert(entries.contains(Paths.get("com/mypackage/common/Header.tpl.html")))
        assert(entries.contains(Paths.get("com/mypackage/SubTest.tpl.html")))
        assert(entries.contains(Paths.get("Test.tpl.html")))
        assert(!entries.contains(Paths.get("com/randomfile.tpl.htm")))
    }
}