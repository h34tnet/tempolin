package net.h34t.tempolin.code

import com.tschuchort.compiletesting.KotlinCompilation
import com.tschuchort.compiletesting.SourceFile
import net.h34t.tempolin.compiler.AstBuilder
import net.h34t.tempolin.compiler.KotlinGenerator
import net.h34t.tempolin.compiler.Tokenizer
import org.junit.jupiter.api.Test
import java.io.FileNotFoundException

class CodeExecutionTest {

    private val tokenizer = Tokenizer()
    private val astBuilder = AstBuilder()
    private val generator = KotlinGenerator()

    private fun generate(
        fileName: String = "TestClass.kt",
        className: String = "TestClass",
        packageName: String = "",
        source: String
    ): SourceFile {
        val tokens = tokenizer.tokenize(source)
        val ast = astBuilder.build(tokens)

        return StringBuilder().let { sb ->
            val fileSpec = generator.generateFile(fileName, className, packageName, ast)
            fileSpec.writeTo(sb)

            SourceFile.kotlin(fileName, sb.toString())
        }
    }

    private fun getTemplateSourceFile(): SourceFile {
        val source = this::class.java.classLoader.getResourceAsStream("TempolinTemplateKt")
            ?.use { stream -> String(stream.readAllBytes()) } ?: throw FileNotFoundException("template not found")
        return SourceFile.kotlin("TempolinTemplate.kt", source)
    }

    @Test
    fun `test code execution literals`() {
        val source = generate(source = "foo bar")

        val result = KotlinCompilation().apply {
            sources = listOf(getTemplateSourceFile(), source)
            messageOutputStream = System.out
        }.compile()

        val testClass = result.classLoader.loadClass("TestClass")
        val instance = testClass.getDeclaredConstructor().newInstance()

        val output = StringBuilder().let { sb ->
            val method = testClass.getMethod("writeTo", Appendable::class.java)
            method.invoke(instance, sb)
            sb.toString()
        }

        assert(output == "foo bar") { "The output is unexpected: \"$output\"" }
    }

    @Test
    fun `test code execution values`() {
        val source = generate(source = "one {\$val} three")

        val result = KotlinCompilation().apply {
            sources = listOf(getTemplateSourceFile(), source)
            messageOutputStream = System.out
        }.compile()

        val testClass = result.classLoader.loadClass("TestClass")
        val instance = testClass.getDeclaredConstructor(String::class.java).newInstance("two")

        val output = StringBuilder().let { sb ->
            val method = testClass.getMethod("writeTo", Appendable::class.java)
            method.invoke(instance, sb)
            sb.toString()
        }

        assert(output == "one two three") { "The output is unexpected: \"$output\"" }
    }

    @Test
    fun `test code execution conditional`() {
        val source = generate(source = "{if \$cond}foo{else}bar{/if}")

        val result = KotlinCompilation().apply {
            sources = listOf(getTemplateSourceFile(), source)
            messageOutputStream = System.out
        }.compile()

        val testClass = result.classLoader.loadClass("TestClass")
        val instance = testClass.getDeclaredConstructor(Boolean::class.java).newInstance(true)

        val output = StringBuilder().let { sb ->
            val method = testClass.getMethod("writeTo", Appendable::class.java)
            method.invoke(instance, sb)
            sb.toString()
        }

        assert(output == "foo") { "The conditional output is unexpected: \"$output\"" }

        val instance2 = testClass.getDeclaredConstructor(Boolean::class.java).newInstance(false)

        val output2 = StringBuilder().let { sb ->
            val method = testClass.getMethod("writeTo", Appendable::class.java)
            method.invoke(instance2, sb)
            sb.toString()
        }

        assert(output2 == "bar") { "The alternative output is unexpected: \"$output\"" }
    }

    @Test
    fun `test code execution one-line comment`() {
        val source = generate(source = "foo{comment}boo baz{/comment}bar")

        val result = KotlinCompilation().apply {
            sources = listOf(getTemplateSourceFile(), source)
            messageOutputStream = System.out
        }.compile()

        val testClass = result.classLoader.loadClass("TestClass")
        val instance = testClass.getDeclaredConstructor().newInstance()

        val output = StringBuilder().let { sb ->
            val method = testClass.getMethod("writeTo", Appendable::class.java)
            method.invoke(instance, sb)
            sb.toString()
        }

        assert(output == "foobar") { "The output is unexpected: \"$output\"" }
    }

    @Test
    fun `test code execution one-line literal`() {
        val source = generate(source = "foo{literal}boo {${'$'}bar} baz{/literal}bar")

        val result = KotlinCompilation().apply {
            sources = listOf(getTemplateSourceFile(), source)
            messageOutputStream = System.out
        }.compile()

        val testClass = result.classLoader.loadClass("TestClass")
        val instance = testClass.getDeclaredConstructor().newInstance()

        val output = StringBuilder().let { sb ->
            val method = testClass.getMethod("writeTo", Appendable::class.java)
            method.invoke(instance, sb)
            sb.toString()
        }

        assert(output == "fooboo {${'$'}bar} bazbar") { "The output is unexpected: \"$output\"" }
    }

// I can't get this to run as I don't know how to access the future instance of testClass in its own constructor.
//    @Test
//    fun `test code execution for block white space removal`() {
//        val source = generate(
//            source = """
//            |<ul>
//            |  {for ${'$'}block}
//            |  <li>{${'$'}value}</li>
//            |  {/for}
//            |</ul>
//        """.trimMargin()
//        )
//
//        val result = KotlinCompilation().apply {
//            sources = listOf(getTemplateSourceFile(), source)
//            messageOutputStream = System.out
//        }.compile()
//
//        val numbers = sequenceOf("one", "two")
//
//        val testClass = result.classLoader.loadClass("TestClass")
//        val blockClass = result.classLoader.loadClass("TestClass${'$'}BlockBlock")
//
//        val instance = testClass.getConstructor()
//            .newInstance({ numbers.map { num -> blockClass.getConstructor().newInstance(testClassInstance !?!?, num) } })
//
//        val output = StringBuilder().let { sb ->
//            val method = testClass.getMethod("writeTo", Appendable::class.java)
//            method.invoke(instance, sb)
//            sb.toString()
//        }
//
//        assert(
//            output == """
//            |<ul>
//            |  <li>one</li>
//            |  <li>two</li>
//            |</ul>
//        """.trimMargin()
//        ) { "The output is unexpected: \"$output\"" }
//    }
}