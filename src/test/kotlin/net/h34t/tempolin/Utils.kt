package net.h34t.tempolin

private val nl = Regex("\r\n|\r")

/**
 * Replaces windows (\r\n) with unix (\n) newlines.
 */
fun String.normalizeNewlines() = this.replace(nl, "\n")