package net.h34t.tempolin.compiler

import net.h34t.tempolin.normalizeNewlines
import org.junit.jupiter.api.Test

/**
 * If there are control structures like conditionals or loops and otherwise only whitespace on a line, discard the
 * whitespace.
 */
class TokenizerWhitespaceLineTest {

    @Test
    fun `test for single line`() {
        val source = """
            |   {for ${'$'}foo}
            |       {${'$'}bar}
            |   {/for}
        """.trimMargin()

        val tokens = Tokenizer().tokenize(source)

        assert(tokens[0] is Token.For)
        tokens[1].let { assert(it is Token.Literal && it.value == "       ") }
        assert(tokens[2] is Token.Value)
        tokens[3].let { assert(it is Token.Literal && it.value == "\n") }
        assert(tokens[4] is Token.EndFor)
        assert(tokens.size == 5)
    }

    @Test
    fun `test if single line`() {
        val source = """
            |{if ${'$'}foo}
            |foo
            |{else}
            |bar
            |{/if}
        """.trimMargin().normalizeNewlines()

        val tokens = Tokenizer().tokenize(source)

        assert(tokens.size == 5)
        assert(tokens[0] is Token.Conditional)
        tokens[1].let { assert(it is Token.Literal && it.value == "foo\n") }
        assert(tokens[2] is Token.Else)
        tokens[3].let { assert(it is Token.Literal && it.value == "bar\n") }
        assert(tokens[4] is Token.EndIf)
    }

    @Test
    fun `test if for nested`() {
        val source = """
            |  {for ${'$'}boo} 
            |    {if ${'$'}foo}
            |      foo
            |    {else}
            |bar
            |    {/if}
            |  {/for}
        """.trimMargin().normalizeNewlines()

        val tokens = Tokenizer().tokenize(source)

        assert(tokens.size == 7)
        assert(tokens[0] is Token.For)
        assert(tokens[1] is Token.Conditional)
        tokens[2].let { assert(it is Token.Literal && it.value == "      foo\n") }
        assert(tokens[3] is Token.Else)
        tokens[4].let { assert(it is Token.Literal && it.value == "bar\n") }
        assert(tokens[5] is Token.EndIf)
        assert(tokens[6] is Token.EndFor)
    }
}