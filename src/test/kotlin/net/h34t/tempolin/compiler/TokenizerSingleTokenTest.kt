package net.h34t.tempolin.compiler

import org.junit.jupiter.api.Test

class TokenizerSingleTokenTest {

    @Test
    fun `test single literal`() {
        val t = Tokenizer()
        val tokens = t.tokenize("x")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.Literal)
        assert((tokens[0] as Token.Literal).value == "x")
    }

    @Test
    fun `test single value`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{\$foo}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.Value)
        assert((tokens[0] as Token.Value).name == "foo")
    }

    @Test
    fun `test single for`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{for \$foo}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.For)
        assert((tokens[0] as Token.For).name == "foo")
    }

    @Test
    fun `test single end for`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{/for}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.EndFor)
    }

    @Test
    fun `test single if`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{if \$foo}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.Conditional)
        assert((tokens[0] as Token.Conditional).condition == "foo")
    }

    @Test
    fun `test single else if`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{else}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.Else)
    }

    @Test
    fun `test single end if`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{/if}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.EndIf)
    }

    @Test
    fun `test single subtemplate`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{template \$name}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.SubTemplate)
        assert((tokens[0] as Token.SubTemplate).name == "name")
    }

    @Test
    fun `test value modifiers star`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{*\$hello}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.Value)
        assert((tokens[0] as Token.Value).modifiers == listOf("*"))
    }

    @Test
    fun `test value modifiers mod`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{\$hello|myModifier}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.Value)
        assert((tokens[0] as Token.Value).modifiers == listOf("myModifier"))
    }

    @Test
    fun `test value modifiers two mod`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{\$hello|myFirstModifier|mySecondModifier}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.Value)
        assert((tokens[0] as Token.Value).name == "hello")
        assert((tokens[0] as Token.Value).modifiers == listOf("myFirstModifier", "mySecondModifier"))
    }

    @Test
    fun `test value modifiers star mod`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{*\$hello|myModifier}")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.Value)
        assert((tokens[0] as Token.Value).modifiers == listOf("myModifier", "*"))
    }
}