package net.h34t.tempolin.compiler

import kotlin.test.Test

class TempolinTest {

    @Test
    fun `test processing`() {
        val tempolin = Tempolin()

        tempolin.process(
            "src/test/resources/tpl",
            "build/generated-sources/tempolin",
            "kotlin",
            // fileFilter = { it.name.endsWith(".tpl.html") }
        ) {
            println(it)
        }
    }
}