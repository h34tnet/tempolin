package net.h34t.tempolin.compiler

import org.junit.jupiter.api.Test

class TokenizerTest {

    @Test
    fun `test empty`() {
        val t = Tokenizer()
        val tokens = t.tokenize("")
        assert(tokens.isEmpty())
    }

    @Test
    fun `test tokenizer literal value mix`() {
        val t = Tokenizer()
        val tokens = t.tokenize("hello {\$name}! Welcome to {\$place}.")
        assert(tokens.size == 5)

        assert(tokens[0] is Token.Literal && (tokens[0] as Token.Literal).value == "hello ")
        assert(tokens[1] is Token.Value && (tokens[1] as Token.Value).name == "name")
        assert(tokens[2] is Token.Literal && (tokens[2] as Token.Literal).value == "! Welcome to ")
        assert(tokens[3] is Token.Value && (tokens[3] as Token.Value).name == "place")
        assert(tokens[4] is Token.Literal && (tokens[4] as Token.Literal).value == ".")
    }

    @Test
    fun `test double value`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{\$hello}{\$world}")
        assert(tokens.size == 2)
        assert(tokens[0] is Token.Value)
        assert(tokens[1] is Token.Value)
    }

    @Test
    fun `test for block`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{for \$variable}literal1{\$value}literal2{/for}")
        assert(tokens.size == 5) { "Wrong token list size: ${tokens.size} instead of 5" }
        assert(tokens[0] is Token.For) { "First token is not a FOR token" }
        assert((tokens[0] as Token.For).name == "variable") { "For token has the wrong name" }
    }

    @Test
    fun `test if condition`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{if \$condition}{\$value}{else}literal{/if}")
        assert(tokens.size == 5) { "Wrong token list size: ${tokens.size} instead of 5" }
        assert(tokens[0] is Token.Conditional) { "First token is not a Conditional token" }
        assert((tokens[0] as Token.Conditional).condition == "condition") { "Conditional token has the wrong name" }
    }
}