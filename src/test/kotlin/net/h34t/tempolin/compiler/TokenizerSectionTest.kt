package net.h34t.tempolin.compiler

import org.junit.jupiter.api.Test

class TokenizerSectionTest {

    @Test
    fun `test single line literal section`() {
        val t = Tokenizer()
        val tokens = t.tokenize("foo {literal}{${'$'}value}{/literal} bar")
        assert(tokens.size == 3)
        (tokens[0] as Token.Literal).let { token ->
            assert(token.value == "foo ") { "Wrong value ${token.value}" }
        }
        (tokens[1] as Token.Literal).let { token ->
            assert(token.value == "{${'$'}value}") { "Wrong value ${token.value}" }
        }
        (tokens[2] as Token.Literal).let { token ->
            assert(token.value == " bar") { "Wrong value ${token.value}" }
        }
    }

    @Test
    fun `test multi line literal section`() {
        val lines = listOf(
            "foo {literal} bar\n",
            "{\$value}\n",
            "baz{/literal} end\n"
        )

        val t = Tokenizer()
        val tokens = lines.flatMapIndexed { num, line -> t.tokenize(line, num) }

        assert(tokens.size == 5)
        assert((tokens[0] as Token.Literal).value == "foo ")
        assert((tokens[1] as Token.Literal).value == " bar\n")
        assert((tokens[2] as Token.Literal).value == "{\$value}\n")
        assert((tokens[3] as Token.Literal).value == "baz")
        assert((tokens[4] as Token.Literal).value == " end\n")
    }

    @Test
    fun `test single line comment section`() {
        val t = Tokenizer()
        val tokens = t.tokenize("foo {comment}{\$value}{/comment} bar")
        assert(tokens.size == 2)
        assert(tokens[0] is Token.Literal)
        assert((tokens[0] as Token.Literal).value == "foo ")
        assert(tokens[1] is Token.Literal)
        assert((tokens[1] as Token.Literal).value == " bar")

    }

    @Test
    fun `test multi line comment section`() {
        val lines = listOf(
            "foo {comment} bar\n",
            "{\$value}", "baz {/comment} end\n"
        )

        val t = Tokenizer()
        val tokens = lines.flatMapIndexed { num, line -> t.tokenize(line, num) }

        assert(tokens.size == 2)
        assert(tokens[0] is Token.Literal)
        assert((tokens[0] as Token.Literal).value == "foo ")
        assert(tokens[1] is Token.Literal)
        assert((tokens[1] as Token.Literal).value == " end\n")
    }
}