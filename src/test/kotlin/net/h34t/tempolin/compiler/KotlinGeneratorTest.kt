package net.h34t.tempolin.compiler

import net.h34t.tempolin.compiler.LineSplitter.Companion.linesnl
import org.junit.jupiter.api.Test
import java.nio.file.Path
import java.nio.file.Paths

/**
 * As it's not straight forward to test the resulting code, we just expect no
 * exceptions.
 */
class KotlinGeneratorTest {

    private val outputPath = Paths.get(".")

    private fun tf(inputPath: String): Path =
        Paths.get(inputPath)

    @Test
    fun `test code generation empty`() {
        val source = ""
        val tokens = Tokenizer().tokenize(source)
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("foo/Foo"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code)
    }

    @Test
    fun `test code generation simple`() {
        val source = "hello"
        val tokens = Tokenizer().tokenize(source)
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("foo/Foo"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code)
    }

    @Test
    fun `test code generation value`() {
        val source = "hello {\$value} baz {\$foo} end"
        val tokens = Tokenizer().tokenize(source)
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("foo/Foo"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code)
    }

    @Test
    fun `test code generation conditional`() {
        val source = "hello {\$value} baz {if \$cond}{\$conseq}{/if} end"
        val tokens = Tokenizer().tokenize(source)
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("foo/Foo"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code)
    }

    @Test
    fun `test code generation block`() {
        val source = "{for \$records}{\$name}{/for}"
        val tokens = Tokenizer().tokenize(source)
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("foo/Foo"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code)
    }

    @Test
    fun `test code generation everything`() {
        val source = "hello, {\$name}!{if \$boo}{for \$records}{\$name}{/for}{else}{\$bombo}{/if}{template \$subtpl}"
        val tokens = Tokenizer().tokenize(source)
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("foo/Foo"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code)
    }

    @Test
    fun `test code generation for modifiers`() {
        val source = "literal {\$value} {*\$value} {\$value|modifier1} {*\$value|modifier2} {*\$value|modifier1|modifier3|modifier4}"
        val tokens = Tokenizer().tokenize(source)
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("ModifierTest"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code)
    }

    @Test
    fun `test double blocks`() {
        val source = """
            |<h2>Entities</h2>
            |{if ${'$'}entities}
            |<ul>
            |    {for ${'$'}entities}
            |    <li><a href="/entity/{#${'$'}id}">{*${'$'}name}</a></li>
            |    {/for}
            |</ul>
            |{/if}
            |
            |<fieldset>
            |    <legend>Create new entity</legend>
            |    <form action="/entity" method="POST">
            |        <select name="type">
            |            {for ${'$'}entityTypes}
            |            <option value="{*${'$'}typeName}">{${'$'}displayName|capitalize}</option>
            |            {/for}
            |        </select>
            |        <input name="name" type="text" placeholder="name">
            |        <input type="submit" value="add">
            |    </form>
            |</fieldset>            
        """.trimMargin()
        val tokenizer = Tokenizer()
        val tokens = source.linesnl().flatMap { line -> tokenizer.tokenize(line) }
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("foo/BlockTest.html"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code)
    }

    @Test
    fun `test readme example`() {
        val source = """
            |<h1>{*${'$'}headline}</h1>
            |{if ${'$'}showTeaser}
            |<p class="teaser">{*${'$'}teaser|ellipsize}</p>
            |{/if}
            |<ul>
            |{for ${'$'}bullets}<li>{*${'$'}text}{/for}
            |</ul>            
        """.trimMargin()

        val tokenizer = Tokenizer()
        val tokens = source.linesnl().flatMap { line -> tokenizer.tokenize(line) }
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("foo/Index.html"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code)
    }

    @Test
    fun `test multiline example`() {
        val source = """
            |foo
            |bar
        """.trimMargin()

        val tokenizer = Tokenizer()
        val tokens = source.linesnl().flatMapIndexed { num, line -> tokenizer.tokenize(line, num) }
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("foo/MultiLine"), outputPath, ast)

        assert(code.contents.isNotBlank())

        println(code.contents)
    }


    @Test
    fun `test function`() {
        val source = """
            |{%myFunc:mystringarg}
        """.trimMargin()


        val tokenizer = Tokenizer()
        val tokens = source.lines().flatMap { line -> tokenizer.tokenize(line) }
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("Index"), outputPath, ast)

        assert(code.contents.isNotBlank())
        println(code.contents)
    }

    @Test
    fun `test function and modifier`() {
        val source = """
            |{*%myFunc:mystringarg|myModifier}
        """.trimMargin()


        val tokenizer = Tokenizer()
        val tokens = source.lines().flatMap { line -> tokenizer.tokenize(line) }
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("Index"), outputPath, ast)

        assert(code.contents.isNotBlank())
        println(code.contents)
    }

    @Test
    fun `test function and modifier same name`() {
        val source = """
            |{%myFunc:mystringarg|myFunc}
            |{${"$"}boo|myFunc}
        """.trimMargin()


        val tokenizer = Tokenizer()
        val tokens = source.lines().flatMap { line -> tokenizer.tokenize(line) }
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator().generateFile(tf("Index"), outputPath, ast)

        assert(code.contents.isNotBlank())
        println(code.contents)
    }
}