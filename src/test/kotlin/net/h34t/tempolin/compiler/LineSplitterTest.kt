package net.h34t.tempolin.compiler

import org.junit.jupiter.api.Test

class LineSplitterTest {

    @Test
    fun `test line reader single line`() {
        val foo = "foo bar baz"

        val res = LineSplitter().split(foo)

        println(res)

        assert(res.count() == 1)
        assert(res.first() == "foo bar baz")
    }

    @Test
    fun `test line reader multi line`() {
        val foo = "foo bar\nbaz"

        val res = LineSplitter().split(foo)

        assert(res.count() == 2)
        assert(res.elementAt(0) == "foo bar\n")
        assert(res.elementAt(1) == "baz")

        println(res)
    }

    @Test
    fun `test line reader multi line trailer`() {
        val foo = "foo bar\nbaz\n"

        val res = LineSplitter().split(foo)

        println(res)

        assert(res.count() == 2)
        assert(res.elementAt(0) == "foo bar\n")
        assert(res.elementAt(1) == "baz\n")
    }

    @Test
    fun `test line reader multi line long trailer`() {
        val foo = "foo bar\nbaz\n\n\n"

        val res = LineSplitter().split(foo)

        println(res)

        assert(res.count() == 4)
        assert(res.elementAt(0) == "foo bar\n")
        assert(res.elementAt(1) == "baz\n")
    }

    @Test
    fun `test line reader multi line long trailer mixed`() {
        val foo = "foo bar\r\nbaz\n\r\n\n"

        val res = LineSplitter().split(foo)

        println(res)

        assert(res.count() == 4)
        assert(res.elementAt(0) == "foo bar\r\n")
        assert(res.elementAt(1) == "baz\n")
        assert(res.elementAt(2) == "\r\n")
        assert(res.elementAt(3) == "\n")
    }
    @Test
    fun `test line reader multi line unicode 0085`() {
        val foo = "foo bar\u0085baz\u0085\u0085"

        val res = LineSplitter().split(foo)

        println(res)

        assert(res.count() == 3)
        assert(res.elementAt(0) == "foo bar\u0085")
        assert(res.elementAt(1) == "baz\u0085")
        assert(res.elementAt(2) == "\u0085")
    }
}