package net.h34t.tempolin.compiler

import org.junit.jupiter.api.Test

class TokenizerEdgeCaseTest {

    @Test
    fun `test brackets`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{")
        assert(tokens.size == 1)
        assert(tokens[0] is Token.Literal)
        assert((tokens[0] as Token.Literal).value == "{")
    }

    @Test
    fun `test incomplete`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{if")
        assert(tokens.size == 2)
        assert(tokens[0] is Token.Literal)
        assert((tokens[0] as Token.Literal).value == "{")
        assert(tokens[1] is Token.Literal)
        assert((tokens[1] as Token.Literal).value == "if")
    }

    @Test
    fun `test unknown control`() {
        val t = Tokenizer()
        val tokens = t.tokenize("{when}foo{/when}")
        assert(tokens.size == 4)
        assert(tokens[0] is Token.Literal)
        assert((tokens[0] as Token.Literal).value == "{")
        assert(tokens[1] is Token.Literal)
        assert((tokens[1] as Token.Literal).value == "when}foo")
        assert(tokens[2] is Token.Literal)
        assert((tokens[2] as Token.Literal).value == "{")
        assert(tokens[3] is Token.Literal)
        assert((tokens[3] as Token.Literal).value == "/when}")
    }

    @Test
    fun `test unmatching comment block`() {
        val tokens = Tokenizer().tokenize("{comment}")
        assert(tokens.isEmpty())
    }

    @Test
    fun `test unmatching comment block with literal`() {
        val tokens = Tokenizer().tokenize("{comment}foo")
        assert(tokens.isEmpty())
    }

    @Test
    fun `test unmatching literal block`() {
        val tokens = Tokenizer().tokenize("{literal}")
        assert(tokens.isEmpty())
    }

    @Test
    fun `test unmatching literal block with literal`() {
        val tokens = Tokenizer().tokenize("{literal}foo")
        assert(tokens.size == 1)
        assert((tokens.first() as Token.Literal).value == "foo")
    }

    @Test
    fun `test multiline linebreaks`() {
        val lines = listOf(
            "foo\n",
            "bar\n"
        )

        val t = Tokenizer()
        val tokens = lines.flatMapIndexed { num, line -> t.tokenize(line, num) }

        assert(tokens.size == 2)
        assert(tokens[0] is Token.Literal)
        assert((tokens[0] as Token.Literal).value == "foo\n")
        assert(tokens[1] is Token.Literal)
        assert((tokens[1] as Token.Literal).value == "bar\n")
    }
}