package net.h34t.tempolin.compiler

import org.junit.jupiter.api.assertThrows
import java.nio.file.Paths
import kotlin.test.Test

class TemplateInterfaceTest {

    @Test
    fun `test interface name`() {
        val tokens = Tokenizer().tokenize("hello, world")
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator("foo.bar.TplInterface").generateFile(Paths.get("foo/Foo"), Paths.get("bar"), ast)

        code.contents.let { source ->
            assert(source.contains(Regex("import\\s+foo\\.bar\\.TplInterface")))
            assert(source.contains(Regex("public\\s+class\\s+Foo\\(\\)\\s+:\\s+TplInterface\\s+\\{")))
        }
    }

    @Test
    fun `test interface std name`() {
        val tokens = Tokenizer().tokenize("hello, world")
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator("net.h34t.tempolin.TempolinTemplate").generateFile(
            Paths.get("foo/Foo"),
            Paths.get("bar"),
            ast
        )

        code.contents.let { source ->
            assert(source.contains(Regex("import\\s+net\\.h34t\\.tempolin\\.TempolinTemplate")))
            assert(source.contains(Regex("public\\s+class\\s+Foo\\(\\)\\s+:\\s+TempolinTemplate\\s+\\{")))
        }
    }

    @Test
    fun `test interface no package`() {
        val tokens = Tokenizer().tokenize("hello, world")
        val ast = AstBuilder().build(tokens)
        val code = KotlinGenerator("Tpl").generateFile(Paths.get("foo/Foo"), Paths.get("bar"), ast)

        code.contents.let { source ->
            assert(source.contains(Regex("import\\s+Tpl")))
            assert(source.contains(Regex("public\\s+class\\s+Foo\\(\\)\\s+:\\s+Tpl\\s+\\{")))
        }
    }

    @Test
    fun `test interface no name`() {
        val tokens = Tokenizer().tokenize("hello, world")
        val ast = AstBuilder().build(tokens)

        assertThrows<IllegalArgumentException> {
            KotlinGenerator("").generateFile(Paths.get("foo/Foo"), Paths.get("bar"), ast)
        }
    }
}