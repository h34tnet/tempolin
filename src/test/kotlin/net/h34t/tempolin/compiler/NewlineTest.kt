package net.h34t.tempolin.compiler

import org.junit.jupiter.api.Test
import java.io.BufferedReader
import java.io.InputStreamReader

class NewlineTest {

    @Test
    fun `test empty line`() {
        val t = Tokenizer()
        val tokens = t.tokenize(
            """
            |foo
            |
            |{if ${'$'}boo}bar{/if}
            |
            |baz
        """.trimMargin()
        )

        println(tokens)

        assert(tokens.size == 8) { "Size should be 8 but is ${tokens.size}" }

        val ast = AstBuilder().build(tokens)

        val filespec = KotlinGenerator().generateFile("file", "classname", "package", ast)
        println(filespec)
    }

    @Test
    fun `test empty uci`() {
        val source = this.javaClass.classLoader.getResourceAsStream("config_file")?.use { BufferedReader(InputStreamReader(it)).readLines() } ?: throw Exception("Test file not found")
        val t = Tokenizer()
        val tokens = source.flatMapIndexed { num, line -> t.tokenize(line + "\n", num) }.let { TokenOptimizer().optimize(it) }

        println(tokens)
        assert(tokens.size == 5)

        val ast = AstBuilder().build(tokens)

        val filespec = KotlinGenerator().generateFile("file", "classname", "package", ast)

        println(filespec)
    }
}