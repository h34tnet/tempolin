package net.h34t.tempolin.compiler

import org.gradle.internal.impldep.org.testng.Assert
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class AstBuilderTest {

    @Test
    fun `build simple AST`() {
        val tokens = Tokenizer().tokenize("foo {\$bar} baz", 0)
        println(tokens)
        val astNode = AstBuilder().build(tokens)
        println(astNode)
    }

    @Test
    fun `build conditional AST`() {
        val tokens = Tokenizer().tokenize("foo {if \$bar}boo{else}bar{/if} baz", 0)
        println(tokens)
        val astNode = AstBuilder().build(tokens)
        println(astNode)
    }

    @Test
    fun `error expected for mismatched conditional`() {
        val tokens = Tokenizer().tokenize("foo {if \$bar} baz")

        val ex = assertThrows<AstBuilder.MismatchedBlockException> {
            AstBuilder().build(tokens)
        }

        Assert.assertEquals(ex.token[0], tokens[1])
        assert(ex.token[0] == tokens[1])
    }

    @Test
    fun `error expected for mismatched conditional-else`() {
        val tokens = Tokenizer().tokenize("foo {for \$bar}{else} bar")

        val ex = assertThrows<AstBuilder.MismatchedBlockException> {
            val ast = AstBuilder().build(tokens)
            println(ast)
        }

        assert(ex.token[0] == tokens[2])
        assert(ex.token[1] == tokens[1])
    }

    @Test
    fun `error expected for unmatched literal block`() {
        assertThrows<Tokenizer.MismatchedTokenException> {
            val tokens = Tokenizer().use { it.tokenize("foo {literal} bar") }
            AstBuilder().build(tokens)
        }
    }

    @Test
    fun `error expected for unmatched comment block`() {
        assertThrows<Tokenizer.MismatchedTokenException> {
            val tokens = Tokenizer().use { it.tokenize("foo {comment} bar") }
            AstBuilder().build(tokens)
        }
    }
}