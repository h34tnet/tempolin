package net.h34t.tempolin

@Suppress("unused")
interface TempolinTemplate {

    fun writeTo(appendable: Appendable)

}