package net.h34t.tempolin

import java.nio.file.Path

object PathUtils {

    /**
     * @return The parent directories as a list.
     */
    fun Path.getParentList(): List<String> =
        this.parent?.listParents() ?: emptyList()

    private fun Path.listParents(): List<String> =
        if (parent == null) listOf(fileName.toString())
        else parent.listParents() + listOf(fileName.toString())

}