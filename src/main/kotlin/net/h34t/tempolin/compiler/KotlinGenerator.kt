package net.h34t.tempolin.compiler

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import net.h34t.tempolin.PathUtils.getParentList
import net.h34t.tempolin.TempolinTemplate
import java.nio.file.Path
import java.nio.file.Paths

/**
 * A generator for kotlin class source files.
 */
class KotlinGenerator(
    templateInterface: String = "net.h34t.tempolin.TempolinTemplate",
    private val addTemplateInterface: Boolean = true,
) : Generator {

    init {
        require(templateInterface.isNotBlank())
    }

    private val classNamer = Regex("[a-zA-Z][a-zA-Z\\d_-]+")

    private val templateInterfaceName = ClassName.bestGuess(templateInterface)

    /**
     * Places the Template interface.
     */
    override fun setup(templateDirectory: Path, outputDirectory: Path) {
        if (addTemplateInterface) {
            FileSpec.builder(templateInterfaceName)
                .addAnnotation(getFormatterAnnotation())
                .addType(
                    TypeSpec.interfaceBuilder(templateInterfaceName)
                        .addFunction(
                            FunSpec.builder("writeTo")
                                .addParameter("appendable", Appendable::class)
                                .build()
                        )
                        .build()
                )
                .build()
                .writeTo(outputDirectory)
        }
    }

    override fun generateFile(
        file: Path,
        outputBaseDirectory: Path,
        root: AstNode?
    ): Generator.Result {
        val className = className(file.fileName.toString())
        val directories = file.getParentList()
        val packageName = directories.joinToString(".")
        val filename = "$className.kt"

        val fileSpec = generateFile(filename, className, packageName, root)

        val fw = StringBuilder()
        fileSpec.writeTo(fw)

        val outputPath = if (file.parent != null)
            outputBaseDirectory.resolve(file.parent).resolve(Paths.get(filename))
        else
            outputBaseDirectory.resolve(Paths.get(filename))

        return Generator.Result(
            filename = "$className.kt",
            contents = fw.toString(),
            outputPath = outputPath
        )
    }

    fun generateFile(
        filename: String,
        className: String,
        packageName: String,
        root: AstNode?
    ): FileSpec = FileSpec.builder(packageName, filename)
        .addFileComment("@formatter:off")
        .addType(generateClass(ClassName(packageName, className), root))
        .addAnnotation(getFormatterAnnotation())
        .build()


    fun className(filename: String): String = classNamer.find(filename)?.value?.split(Regex("[^a-zA-Z0-9]+"))
        ?.map { it.replaceFirstChar { fc -> fc.uppercase() } }
        ?.joinToString(separator = "")
        ?: throw Exception("File name $filename can't be used as a class name.")

    fun generateClass(className: ClassName, root: AstNode?, blockClass: Boolean = false): TypeSpec {
        // get all parameters and
        val parameters = extractParameters(root).selectType()

        val paramByName = parameters.associateBy({ it.name }, { it.type })

        // Finds all value modifier names used in this template.
        val modifiers = gatherFromNodes(root) { node ->
            when (node) {
                is AstNode.FunctionNode -> (node.token as Token.Function).modifiers + node.name
                is AstNode.ValueNode -> (node.token as Token.Value).modifiers
                else -> emptyList()
            }
        }.flatten().toSet()

        val hasModifierInterface = modifiers.isNotEmpty()
        val modifierInterface = createModifierInterface(modifiers)

        val code = generateWriteTo(root, paramByName)
        val blocks = extractBlocks(root)

        return TypeSpec.classBuilder(className).run {
            addSuperinterface(templateInterfaceName)

            if (blockClass)
                addModifiers(KModifier.INNER)

            primaryConstructor(
                FunSpec.constructorBuilder().run {
                    // If there are modifiers defined in the values, add a parameter with the modifier interface type.
                    if (hasModifierInterface && !blockClass)
                        addParameter(
                            "modifiers",
                            ClassName(
                                "",
                                modifierInterface.name
                                    ?: throw Exception("No type name in modifierInterface")
                            )
                        )

                    addParameters(parameters.map {
                        when (it.type) {
                            is DataType.Block -> {
                                // blocks must be assigned via a template-scoped block for the Block instances to have
                                // access to the top-level templates modifier instance.
                                val blockLambda = LambdaTypeName.get(
                                    receiver = className,
                                    returnType = it.type.toSequenceType()
                                )
                                ParameterSpec.builder(it.name, blockLambda).build()
                            }
                            is DataType.Template -> {
                                ParameterSpec.builder(it.name, templateInterfaceName).build()
                            }
                            else -> {
                                ParameterSpec.builder(it.name, it.type.toType()).build()
                            }
                        }
                    }).build()
                }
            )

            if (modifiers.isNotEmpty() && !blockClass)
                addProperty(PropertySpec.builder("modifiers", ClassName("", modifierInterface.name ?: throw Exception("No type name in modifierInterface")), KModifier.PRIVATE).initializer("modifiers").build())

            addProperties(parameters.map {
                when (it.type) {
                    is DataType.Block -> {
                        PropertySpec.builder(it.name, it.type.toSequenceType())
                            .initializer("${it.name}.invoke(this)")
                            .addModifiers(KModifier.PRIVATE)
                            .build()
                    }
                    is DataType.Template -> {
                        PropertySpec.builder(it.name, templateInterfaceName)
                            .initializer(it.name)
                            .addModifiers(KModifier.PRIVATE)
                            .build()
                    }
                    else -> {
                        PropertySpec.builder(it.name, it.type.toType())
                            .initializer(it.name)
                            .addModifiers(KModifier.PRIVATE)
                            .build()
                    }
                }
            })

            addFunction(
                FunSpec.builder("writeTo")
                    .addModifiers(KModifier.OVERRIDE)
                    .addParameter(ParameterSpec.builder("appendable", Appendable::class).build())
                    .addCode(code)
                    .build()
            )

            addTypes(blocks)

            if (modifiers.isNotEmpty() && !blockClass)
                addType(modifierInterface)

            if (!blockClass)
                addFunction(createToString())

            build()
        }
    }

    fun extractParameters(ast: AstNode?): List<Type> {
        return when (ast) {
            null -> emptyList()
            is AstNode.Noop,
            is AstNode.LiteralNode,
            is AstNode.FunctionNode -> extractParameters(ast.next)
            is AstNode.ValueNode -> listOf(Type(ast.name, DataType.Str)) + extractParameters(ast.next)
            is AstNode.ConditionalNode -> listOf(Type(ast.name, DataType.Bool)) +
                    extractParameters(ast.consequence) +
                    extractParameters(ast.alternative) +
                    extractParameters(ast.next)

            is AstNode.BlockNode -> listOf(Type(ast.name, DataType.Block(blockName(ast.name)))) + extractParameters(ast.next)
            is AstNode.SubTemplateNode -> listOf(Type(ast.name, DataType.Template)) + extractParameters(ast.next)

        }
    }

    /**
     * Creates the writeTo function body.
     */
    fun generateWriteTo(ast: AstNode?, param: Map<String, DataType>): CodeBlock {
        return when (ast) {
            null -> CodeBlock.builder().build()
            is AstNode.Noop -> generateWriteTo(ast.next, param)
            is AstNode.LiteralNode -> {
                    CodeBlock.builder()
                        .addStatement("appendable.append(%S)", ast.value)
                        .add(generateWriteTo(ast.next, param))
                        .build()
            }

            is AstNode.ValueNode -> {
                CodeBlock.builder()
                    .addStatement(
                        "appendable.append(%L)",
                        wrapVariableInModifiers((ast.token as Token.Value).modifiers, CodeBlock.of("%N", ast.name))
                    )                    .add(generateWriteTo(ast.next, param))
                    .build()
            }

            is AstNode.FunctionNode -> {
                CodeBlock.builder()
                    .addStatement(
                        "appendable.append(%L)",
                        wrapVariableInModifiers(
                            (ast.token as Token.Function).modifiers,
                            CodeBlock.of("%N.%N(%S)", "modifiers", ast.name, ast.argument)
                        )
                    )
                    .add(generateWriteTo(ast.next, param))
                    .build()
            }

            is AstNode.ConditionalNode -> {
                CodeBlock.builder().apply {
                    when (val type = param[ast.name]) {
                        is DataType.Str -> beginControlFlow("if (%N.isNotEmpty())", ast.name)
                        is DataType.Bool -> beginControlFlow("if (%N)", ast.name)
                        // note that if a sequence is used in a conditional, it must be converted to a list for the size to be evaluated
                        is DataType.Block -> beginControlFlow("if (%N.count() > 0)", ast.name)
                        DataType.Template,
                        null -> throw Exception("Wrong datatype $type in conditional for ${ast.name}")
                    }

                    val s = sequenceOf("a", "b")
                    s.count()

                    add(generateWriteTo(ast.consequence, param))
                    nextControlFlow("else")
                    add(generateWriteTo(ast.alternative, param))
                    endControlFlow()
                    add(generateWriteTo(ast.next, param))
                }.build()
            }

            is AstNode.BlockNode -> {
                CodeBlock.builder()
                    .beginControlFlow("%N.forEach { block ->", ast.name)
                    .addStatement("block.writeTo(appendable)")
                    .endControlFlow()
                    .add(generateWriteTo(ast.next, param))
                    .build()
            }

            is AstNode.SubTemplateNode -> {
                CodeBlock.builder()
                    .addStatement("%N.writeTo(appendable)", ast.name)
                    .add(generateWriteTo(ast.next, param))
                    .build()
            }
        }
    }

    fun createToString() = FunSpec.builder("toString")
        .addModifiers(KModifier.OVERRIDE)
        .returns(String::class)
        .addCode(
            CodeBlock.Builder()
                .addStatement("return StringBuilder().apply { writeTo(this) }.toString()")
                .build()
        )
        .build()

    /**
     * Finds all block nodes and generates their classes.
     */
    fun extractBlocks(node: AstNode?): List<TypeSpec> {
        return when (node) {
            null -> emptyList()
            is AstNode.Noop,
            is AstNode.LiteralNode,
            is AstNode.ValueNode,
            is AstNode.FunctionNode,
            is AstNode.SubTemplateNode -> extractBlocks(node.next)

            is AstNode.ConditionalNode ->
                extractBlocks(node.consequence) +
                        extractBlocks(node.alternative) +
                        extractBlocks(node.next)

            is AstNode.BlockNode ->
                listOf(generateClass(ClassName("", blockName(node.name)), node.block, true)) +
                        extractBlocks(node.next)
        }
    }

    /**
     * The names and types of named tokens (values, blocks, conditional, templates, ...).
     */
    class Type(
        val name: String,
        val type: DataType
    )

    /**
     * The internal data types of named tokens and their translations to the corresponding kotlin data types.
     */
    sealed class DataType {
        object Str : DataType() {
            override fun toType() = ClassName("kotlin", "String")
        }

        object Bool : DataType() {
            override fun toType() = Boolean::class.java.asTypeName()
        }

        object Template : DataType() {
            override fun toType() = TempolinTemplate::class.asTypeName()
        }

        class Block(private val blockName: String) : DataType() {
            override fun toType() = ClassName("", blockName)

            fun toSequenceType() =
                Sequence::class.asClassName().parameterizedBy(toType())
        }

        abstract fun toType(): TypeName
    }

    /**
     * Creates a class name from a block variable.
     *
     * Example: {for $myList} returns "MyListBlock".
     */
    fun blockName(name: String) = name.replaceFirstChar { it.uppercase() } + "Block"

    /**
     * Removes duplicate Type names from the list by selecting the DataType with the highest ranking.
     */
    fun List<Type>.selectType(): List<Type> =
        this
            .groupBy({ x -> x.name }, { x -> x.type })
            .entries.map { Type(it.key, it.value.select()) }

    /**
     * Selects the DataType with the highest ranking.
     *
     * Example: if $foo is used as a block variable and as a conditional, the block variable takes precedence and the
     * variable is used in the conditional as foo.isNotEmpty()
     */
    fun List<DataType>.select(): DataType {
        val block = this.find { it is DataType.Block }
        if (block != null) return block

        val str = this.find { it is DataType.Str }
        if (str != null) return str

        return this.first()
    }

    fun <T> gatherFromNodes(astNode: AstNode?, func: (AstNode) -> T): List<T> = when (astNode) {
        null -> emptyList()
        is AstNode.FunctionNode,
        is AstNode.ValueNode,
        is AstNode.LiteralNode,
        is AstNode.Noop,
        is AstNode.SubTemplateNode -> listOf(func(astNode)) + gatherFromNodes(astNode.next, func)

        is AstNode.BlockNode -> gatherFromNodes(astNode.block, func) + gatherFromNodes(astNode.next, func)

        is AstNode.ConditionalNode -> gatherFromNodes(astNode.consequence, func) +
                gatherFromNodes(astNode.alternative, func) +
                gatherFromNodes(astNode.next, func)
    }

    /**
     * Creates and interface with method definitions for all value modifiers used in this template.
     */
    fun createModifierInterface(modifiers: Set<String>) =
        TypeSpec.interfaceBuilder("Modifiers")
            .addFunctions(modifiers.map {
                FunSpec.builder(translateModifierName(it))
                    .addModifiers(KModifier.ABSTRACT)
                    .addParameter("arg", String::class)
                    .returns(String::class)
                    .build()
            })
            .build()

    fun wrapVariableInModifiers(modifiers: List<String>, variable: CodeBlock): CodeBlock =
        if (modifiers.isEmpty()) {
            variable
        } else {
            CodeBlock.of(
                "modifiers.%N(%L)",
                translateModifierName(modifiers.last()),
                wrapVariableInModifiers(modifiers.dropLast(1), variable)
            )
        }

    /**
     * Adds file annotations to suppress kotlin style analyzer complaints (which are pointless for generated code).
     */
    fun getFormatterAnnotation() =
        AnnotationSpec.builder(Suppress::class)
            .useSiteTarget(AnnotationSpec.UseSiteTarget.FILE)
            .addMember("%S", "RedundantVisibilityModifier")
            .addMember("%S", "UnusedImport")
            .addMember("%S", "LocalVariableName")
            .addMember("%S", "PropertyName")
            .addMember("%S", "unused")
            .addMember("%S", "SpellCheckingInspection")
            .addMember("%S", "ControlFlowWithEmptyBody")
            .addMember("%S", "RedundantUnitReturnType")
        .build()
}