package net.h34t.tempolin.compiler

sealed class AstNode(val token: Token?) {

    var next: AstNode? = null

    abstract fun toString(ident: Int): String

    class Noop : AstNode(null) {
        override fun toString() = toString(0)
        override fun toString(ident: Int) = " ".repeat(ident) + "Noop\n" + next?.toString(ident)
    }

    class LiteralNode(val value: String, token: Token.Literal) : AstNode(token) {
        override fun toString() = toString(0)
        override fun toString(ident: Int) = " ".repeat(ident) + "Literal \"$value\"\n" + next?.toString(ident)
    }

    class ValueNode(val name: String, token: Token.Value) : AstNode(token) {
        override fun toString() = toString(0)
        override fun toString(ident: Int) = " ".repeat(ident) + "Value \"$name\"\n" + next?.toString(ident)
    }

    class FunctionNode(val name: String, val argument: String, token: Token.Function) : AstNode(token) {
        override fun toString(ident: Int) = " ".repeat(ident) + "Function \"$name\"\n" + next?.toString(ident)

        override fun toString() = toString(0)

    }

    class ConditionalNode(val name: String, token: Token.Conditional) : AstNode(token) {
        var consequence: AstNode? = null
        var alternative: AstNode? = null

        override fun toString() = toString(0)
        override fun toString(ident: Int) = " ".repeat(ident) + "Conditional \"$name\"\n" +
                " ".repeat(ident) + "- consequence:\n" +
                (consequence?.toString(ident + 2) ?: "") + "\n" +
                " ".repeat(ident) + "- alternative:\n" +
                alternative?.toString(ident + 2) + "\n" +
                " ".repeat(ident) +
                next?.toString(ident)
    }

    class BlockNode(val name: String, token: Token.For) : AstNode(token) {
        var block: AstNode? = null
        override fun toString() = toString(0)
        override fun toString(ident: Int) = " ".repeat(ident) + "Block \"$name\"\n" +
                block?.toString(ident + 2) +
                next?.toString(ident)
    }

    class SubTemplateNode(val name: String, token: Token.SubTemplate) : AstNode(token) {
        override fun toString() = toString(0)
        override fun toString(ident: Int) = " ".repeat(ident) + "Name \"$name\"\n" + next?.toString(ident)
    }
}