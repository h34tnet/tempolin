package net.h34t.tempolin.compiler

class AstBuilder {

    /**
     * Builds an Abstract Syntax Tree from the list of tokens.
     */
    fun build(tokens: List<Token>): AstNode {
        val root: AstNode = AstNode.Noop()
        var previous: AstNode = root
        var current: AstNode = root

        // for jumping to the currently active block
        val stack = ArrayDeque<AstNode>()

        tokens.forEach { token ->
            when (token) {
                is Token.Literal -> {
                    current = AstNode.LiteralNode(token.value, token)
                    previous.next = current
                }

                is Token.Value -> {
                    current = AstNode.ValueNode(token.name, token)
                    previous.next = current
                }

                is Token.Function -> {
                    current = AstNode.FunctionNode(token.name, token.argument, token)
                    previous.next = current
                }

                is Token.Conditional -> {
                    val conditional = AstNode.ConditionalNode(token.condition, token)
                    previous.next = conditional
                    val consequence = AstNode.Noop()
                    consequence.let {
                        conditional.consequence = it
                        current = it
                    }
                    stack.addLast(conditional)
                }

                is Token.Else -> {
                    val conditional = stack.last()
                    if (conditional !is AstNode.ConditionalNode) {
                        throw MismatchedBlockException(
                            "{else} without matching {if \$...} at ${token.errorDescription()}",
                            token,
                            conditional.token
                        )
                    }
                    val alternative = AstNode.Noop()
                    alternative.let {
                        conditional.alternative = it
                        current = it
                    }
                }

                is Token.EndIf -> {
                    val conditional = stack.removeLast()
                    if (conditional !is AstNode.ConditionalNode) {
                        throw MismatchedBlockException(
                            "{/if} without matching {if \$...} at ${token.errorDescription()} against ${(conditional.token?.errorDescription() ?: "none")}",
                            token,
                            conditional.token
                        )
                    }
                    current = conditional
                }

                is Token.For -> {
                    val block = AstNode.BlockNode(token.name, token)
                    previous.next = block
                    stack.addLast(block)
                    val nop = AstNode.Noop()
                    block.block = nop
                    current = nop
                }

                is Token.EndFor -> {
                    val block = stack.removeLast()
                    if (block !is AstNode.BlockNode)
                        throw MismatchedBlockException(
                            "{/for} without matching opening {for \$...} at ${token.errorDescription()} against " + (block.token?.errorDescription()
                                ?: "none"), token, block.token
                        )
                    current = block
                }

                is Token.SubTemplate -> {
                    current = AstNode.SubTemplateNode(token.name, token)
                    previous.next = current
                }
            }
            previous = current
        }

        // raise an error if there are unclosed blocks left
        if (stack.isNotEmpty()) {
            stack.last().token?.let {
                throw MismatchedBlockException("${it.raw} was not closed at ${it.errorDescription()}", it)
            }
        }

        return root
    }

    /**
     * Thrown if there are mismatched block tags:
     *
     * 1. conditional else or /if without an opening if
     * 2. /for without an opening for
     * 3. or opening if/for without a respective closing tag
     */
    class MismatchedBlockException(message: String, vararg val token: Token?) : Exception(message)
}