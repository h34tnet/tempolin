package net.h34t.tempolin.compiler

import java.nio.file.Path

interface Generator {

    /**
     * *, #, + and ~ are the special prefix chars allowed by the value token parser.
     * They're replaced with regular function names because they might lead to problems on certain systems.
     */
    fun translateModifierName(mod: String) = when (mod) {
        "*" -> "starPrefix"
        "#" -> "hashPrefix"
        "+" -> "plusPrefix"
        "~" -> "tildePrefix"
        else -> mod
    }

    /**
     * Called once before processing the files.
     */
    fun setup(templateDirectory: Path, outputDirectory: Path)

    /**
     * Generates the source code for a template file.
     *
     * @param file The input file path relative to the input base directory.
     * @param outputBaseDirectory The output base directory.
     * @param root The root AST Node.
     */
    fun generateFile(
        file: Path,
        outputBaseDirectory: Path,
        root: AstNode?
    ): Result

    /**
     * The resulting code.
     *
     * @property filename The filename to store the contents in.
     * @property contents The generated source code.
     */
    data class Result(
        val filename: String,
        val outputPath: Path,
        val contents: String,
    )
}