package net.h34t.tempolin.compiler

sealed class Token(val raw: String, val line: Int, val pos: IntRange) {

    class Literal(val value: String, line: Int, pos: IntRange) : Token(value, line, pos) {
        override fun toString() = super.getPos() + " LITERAL \"$value\""
    }

    class Value(val name: String, val modifiers: List<String>, raw: String, line: Int, pos: IntRange) :
        Token(raw, line, pos) {
        override fun toString() = super.getPos() + " VALUE ($name) " + modifiers.joinToString(", ") { "\"$it\"" }
    }

    class Function(
        val name: String,
        val argument: String,
        val modifiers: List<String>,
        raw: String,
        line: Int,
        pos: IntRange
    ) : Token(raw, line, pos) {
        override fun toString() =
            super.getPos() + " FUNCTION ($name(\"$argument\")) " + modifiers.joinToString(", ") { "\"$it\"" }
    }

    class Conditional(val condition: String, raw: String, line: Int, pos: IntRange) : Token(raw, line, pos) {
        override fun toString() = super.getPos() + " IF ($condition) "
    }

    class Else(raw: String, line: Int, pos: IntRange) : Token(raw, line, pos) {
        override fun toString() = super.getPos() + " ELSE"
    }

    class EndIf(raw: String, line: Int, pos: IntRange) : Token(raw, line, pos) {
        override fun toString() = super.getPos() + " ENDIF"
    }

    class For(val name: String, raw: String, line: Int, pos: IntRange) : Token(raw, line, pos) {
        override fun toString() = super.getPos() + " FOR ($name)"
    }

    class EndFor(raw: String, line: Int, pos: IntRange) : Token(raw, line, pos) {
        override fun toString() = super.getPos() + " ENDFOR"
    }

    class SubTemplate(val name: String, raw: String, line: Int, pos: IntRange) : Token(raw, line, pos) {
        override fun toString() = super.getPos() + " TEMPLATE ($name)"
    }

    fun getPos() = "(#$line, $pos)"

    fun errorDescription() = "\"$raw\" (line $line, pos ${pos.first}-${pos.last})"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Token) return false

        if (raw != other.raw) return false
        if (line != other.line) return false
        if (pos != other.pos) return false

        return true
    }

    override fun hashCode(): Int {
        var result = raw.hashCode()
        result = 31 * result + line.hashCode()
        result = 31 * result + pos.hashCode()
        return result
    }
}
