package net.h34t.tempolin.compiler

/**
 * Optimizes a list of tokens.
 *
 * Currently, the only optimization is to combine successive literals into one.
 */
class TokenOptimizer {

    /**
     * Attempts to optimize a list of tokens.
     *
     * @param tokens The unoptimized list of tokens.
     *
     * @return The optimized list of tokens.
     */
    fun optimize(tokens: List<Token>): List<Token> {
        val ot = mutableListOf<Token>()

        tokens.forEach { token ->
            if (token is Token.Literal && ot.lastOrNull() is Token.Literal) {
                val ntoken = ot.removeLast()
                val nvalue = (ntoken as Token.Literal).value + token.value

                ot.add(Token.Literal(nvalue, ntoken.line, ntoken.pos.first..token.pos.last))
            } else {
                ot.add(token)
            }
        }

        return ot
    }
}