package net.h34t.tempolin.compiler

import net.h34t.tempolin.compiler.LineSplitter.Companion.linesnl
import java.io.Closeable
import java.nio.file.Path

/**
 * The Tokenizer splits an input line into a list of [Token]s.
 *
 * It employs the close method from the Closable interface to raise an exception in the presence of unclosed comments
 * or literals.
 */
class Tokenizer : Closeable {

    /**
     * An unescaped opening brace, meaning it finds braces with either no or an even number of escaping backslashes
     * preceding it "\".
     */
    private val opener = Regex("""(?!\\(\\\\)*)\{""")

    /**
     * The mode the tokenizer is in.
     */
    enum class Mode {
        Normal,
        Literal,
        Comment
    }

    private var mode = Mode.Normal

    /**
     * Read the contents of a file and tokenize it line-by-line.
     */
    fun tokenize(file: Path): List<Token> =
        LineSplitter().split(file).flatMapIndexed { lineNum, line -> tokenize(line, lineNum) }.toList()

    /**
     * Tokenize a string line-by-line.
     */
    fun tokenize(source: String) =
        source.linesnl().flatMapIndexed { lineNum, line -> tokenize(line, lineNum) }.toList()

    /**
     * Parse a string into a list of tokens.
     *
     * Unknown tokens are ignored and handled as literals.
     */
    fun tokenize(line: String, lineNum: Int = 0): List<Token> {
        val tokens = mutableListOf<Token>()
        var pos = 0

        while (pos < line.length) {
            when (mode) {
                Mode.Normal -> {
                    val nextOpenBrace = opener.find(line, pos)
                    if (nextOpenBrace != null) {
                        if (pos < nextOpenBrace.range.first) {
                            tokens.add(
                                Token.Literal(
                                    line.substring(pos, nextOpenBrace.range.first),
                                    lineNum,
                                    pos until nextOpenBrace.range.first + 1
                                )
                            )
                            pos = nextOpenBrace.range.first
                        }

                        var found = false

                        if (value.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                val prefix = match.groupValues[1]
                                val modifiers = match.groupValues[3].split("|")

                                tokens.add(
                                    Token.Value(
                                        match.groupValues[2],
                                        (modifiers + prefix).filter { it.isNotBlank() },
                                        match.groupValues[0],
                                        lineNum,
                                        match.range
                                    )
                                )

                                pos = match.range.last + 1
                            } != null) found = true

                        if (function.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                val prefix = match.groupValues[1]
                                val modifiers = match.groupValues[4].split("|")

                                tokens.add(
                                    Token.Function(
                                        match.groupValues[2],
                                        match.groupValues[3],
                                        (modifiers + prefix).filter { it.isNotBlank() },
                                        match.groupValues[0],
                                        lineNum,
                                        match.range
                                    )
                                )

                                pos = match.range.last + 1
                            } != null) found = true

                        if (!found && conditional.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                tokens.add(
                                    Token.Conditional(
                                        match.groupValues[1],
                                        match.groupValues[0],
                                        lineNum,
                                        match.range
                                    )
                                )
                                pos = match.range.last + 1
                            } != null) found = true

                        if (!found && conditionalElse.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                tokens.add(
                                    Token.Else(
                                        match.groupValues[0],
                                        lineNum,
                                        match.range
                                    )
                                )
                                pos = match.range.last + 1
                            } != null) found = true

                        if (!found && conditionalEnd.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                tokens.add(
                                    Token.EndIf(
                                        match.groupValues[0],
                                        lineNum,
                                        match.range
                                    )
                                )
                                pos = match.range.last + 1
                            } != null) found = true

                        if (!found && forBlock.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                tokens.add(
                                    Token.For(
                                        match.groupValues[1],
                                        match.groupValues[0],
                                        lineNum,
                                        match.range
                                    )
                                )
                                pos = match.range.last + 1
                            } != null) found = true

                        if (!found && endFor.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                tokens.add(
                                    Token.EndFor(
                                        match.groupValues[0],
                                        lineNum,
                                        match.range
                                    )
                                )
                                pos = match.range.last + 1
                            } != null) found = true

                        if (!found && subTemplate.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                tokens.add(
                                    Token.SubTemplate(
                                        match.groupValues[1],
                                        match.groupValues[0],
                                        lineNum,
                                        match.range
                                    )
                                )
                                pos = match.range.last + 1
                            } != null) found = true

                        if (!found && literal.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                mode = Mode.Literal
                                pos = match.range.last + 1
                            } != null) found = true

                        if (!found && comment.matchAt(line, nextOpenBrace.range.first)?.let { match ->
                                mode = Mode.Comment
                                pos = match.range.last + 1
                            } != null) found = true

                        if (!found) {
                            tokens.add(
                                Token.Literal(
                                    line.substring(pos, nextOpenBrace.range.first + 1),
                                    lineNum,
                                    pos until nextOpenBrace.range.first
                                )
                            )
                            pos = nextOpenBrace.range.last + 1
                        }

                    } else {
                        tokens.add(
                            Token.Literal(
                                line.substring(pos until line.length), lineNum,
                                pos until line.length
                            )
                        )
                        pos = line.length
                    }
                }

                Mode.Literal -> {
                    val mr = literalEnd.find(line, pos)
                    pos = if (mr != null) {
                        mode = Mode.Normal
                        tokens.add(
                            Token.Literal(
                                line.substring(pos until mr.range.first),
                                lineNum,
                                pos until mr.range.first
                            )
                        )
                        mr.range.last + 1
                    } else {
                        tokens.add(
                            Token.Literal(
                                line.substring(pos until line.length), lineNum,
                                pos until line.length
                            )
                        )
                        line.length
                    }
                }

                Mode.Comment -> {
                    if (commentEnd.find(line, pos)?.let { match ->
                            pos = match.range.last + 1
                            mode = Mode.Normal
                        } == null) {
                        pos = line.length
                    }
                }
            }
        }

        // If the line contains only (at least one) control token and (at least one) literal token consisting purely
        // of whitespace, discard the whitespace.
        // TODO re-add line break
        return if (tokens.size > 1 &&
            tokens.all { token ->
                (token is Token.Literal && token.value.isBlank()) ||
                        token is Token.For || token is Token.EndFor ||
                        token is Token.Conditional || token is Token.Else || token is Token.EndIf
            }
        ) {
            tokens.filterNot { token -> token is Token.Literal && token.value.isBlank() }
        } else {
            tokens
        }
    }

    override fun close() {
        if (mode != Mode.Normal)
            throw MismatchedTokenException("Unclosed comment or literal")
    }

    class MismatchedTokenException(msg: String) : Exception(msg)

    private val value = Regex("\\{([*#+~])?\\$([a-z][a-zA-Z\\d]+)((\\|[a-z][a-zA-Z\\d]*)*)}")

    private val function = Regex("\\{([*#+~])?%([a-z][a-zA-Z\\d]+):([a-z][a-zA-Z\\d-_. ]+)((\\|[a-z][a-zA-Z\\d]*)*)}")

    private val conditional = Regex("\\{if\\s+\\$([a-z][a-zA-Z\\d]+)}")
    private val conditionalElse = Regex("\\{else}")
    private val conditionalEnd = Regex("\\{/if}")

    private val forBlock = Regex("\\{for\\s+\\$([a-z][a-zA-Z\\d]+)}")
    private val endFor = Regex("\\{/for}")

    private val subTemplate = Regex("\\{template\\s+\\\$([a-z][a-zA-Z\\d]+)}")

    private val literal = Regex("\\{literal}")
    private val literalEnd = Regex("\\{/literal}")

    private val comment = Regex("\\{comment}")
    private val commentEnd = Regex("\\{/comment}")
}