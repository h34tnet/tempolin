package net.h34t.tempolin.compiler

import java.nio.file.Path
import java.util.regex.Pattern
import kotlin.io.path.readText

/**
 * Splits a string into lines (i.e. delimiter are newline characters) including the line-endings themselves.
 */
class LineSplitter {

    /**
     * Split string into lines preserving the newline characters.
     */
    fun split(input: String): List<String> {
        val matcher = linePattern.matcher(input)
        val res = mutableListOf<String>()

        while (matcher.find()) {
            val mr = matcher.toMatchResult()
            res.add(mr.group())
        }

        return res.filter { it.isNotEmpty() }
    }

    /**
     * Split file contents into lines preserving the newline characters.
     */
    fun split(file: Path): List<String> = split(file.readText())

    companion object {
        private val linePattern = Pattern.compile("[^\\r\\n\u0085\u2028\u2029]*(\\r\\n|\\r|\\n|\u0085|\u2028|\u2029)?")

        fun String.linesnl() = LineSplitter().split(this)
    }
}