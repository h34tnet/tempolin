package net.h34t.tempolin.compiler

import net.h34t.tempolin.TemplateFileCollector
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.text.ParseException

/**
 * Collects files and does the actual compilation work.
 *
 * @throws ParseException if an error occurs during parsing/tokenizing.
 */
class Tempolin {

    fun process(
        templateDirectory: String,
        outputDirectory: String,
        compilationTarget: String,
        runSetup: Boolean = true,
        fileFilter: (File) -> Boolean = { true },
        templateInterface: String = "net.h34t.tempolin.TempolinTemplate",
        addTemplateInterface: Boolean = true,
        logProgress: (String) -> Unit = { println(it) },
    ) {
        val templatePath = Paths.get(templateDirectory)
        val outputPath = Paths.get(outputDirectory)

        val files = TemplateFileCollector().findFilesInDirectory(templatePath, fileFilter)

        val tokenOptimizer = TokenOptimizer()

        val generator = when (compilationTarget) {
            "kotlin" -> KotlinGenerator(templateInterface, addTemplateInterface)
            else -> throw IllegalArgumentException("No generator for target \"$compilationTarget\"")
        }

        if (runSetup)
            generator.setup(templatePath, outputPath)

        files.forEach { file ->
            logProgress.invoke("processing $file ... ")
            val filePath = templatePath.resolve(file)

            val tokens = Tokenizer().use { tokenizer ->
                tokenOptimizer.optimize(tokenizer.tokenize(filePath))
            }

            val ast = AstBuilder().build(tokens)

            val result = generator.generateFile(
                file = file,
                outputBaseDirectory = outputPath,
                root = ast
            )

            Files.createDirectories(result.outputPath.parent)

            Files.writeString(
                result.outputPath,
                result.contents,
                StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING
            )
        }
    }
}