package net.h34t.tempolin

import org.gradle.api.Project
import java.io.File

open class TempolinExtension(@Suppress("UNUSED_PARAMETER") project: Project) {

    val configurations = mutableListOf<TempolinConfig>()

    @Suppress("unused")
    fun add(configuration: TempolinConfig.() -> Unit) {
        configurations.add(TempolinConfig().apply(configuration))
    }

    data class TempolinConfig(
        var templateDirectory: String = "",
        var outputDirectory: String = "",
        var addSources: Boolean = false,
        var compilationTarget: String = "kotlin",
        var fileFilter: ((File) -> Boolean) = { true },
        var templateInterface: String = "net.h34t.tempolin.TempolinTemplate",
        var addTemplateInterface: Boolean = true
    )
}

