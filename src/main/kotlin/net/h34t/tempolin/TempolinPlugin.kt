package net.h34t.tempolin

import net.h34t.tempolin.compiler.Tempolin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logging

@Suppress("unused")
class TempolinPlugin : Plugin<Project> {
    private val logger = Logging.getLogger(TempolinPlugin::class.java)

    override fun apply(project: Project) {
        val extension = project.extensions.create("tempolin", TempolinExtension::class.java, project)

        val tempolin = Tempolin()

        // add sourceSets for every configuration with addSources = true
        project.afterEvaluate {
            val sourceSets =
                project.extensions.getByName("sourceSets") as org.gradle.api.tasks.SourceSetContainer

            extension.configurations.forEach { c ->
                if (c.addSources) {
                    logger.lifecycle("Adding Tempolin sourceSet ${c.outputDirectory}")

                    sourceSets.getByName("main").apply {
                        java.srcDir(c.outputDirectory)
                    }
                } else {
                    logger.debug("Not adding tempolin sourceSet for ${c.outputDirectory}")
                }
            }
        }

        // run tempolin for every configuration
        project.task("tempolin") {
            it.doLast {
                extension.configurations.forEach { c ->
                    tempolin.process(
                        templateDirectory = c.templateDirectory,
                        outputDirectory = c.outputDirectory,
                        compilationTarget = c.compilationTarget,
                        runSetup = c == extension.configurations.first(),
                        fileFilter = c.fileFilter,
                        templateInterface = c.templateInterface,
                        addTemplateInterface = c.addTemplateInterface,
                        ) { msg -> logger.lifecycle(msg) }
                }
            }
        }
    }
}

