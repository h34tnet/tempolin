package net.h34t.tempolin

import net.h34t.tempolin.compiler.Tempolin

class CliRunner {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val tempolin = Tempolin()

            try {
                val templateDirectory = args[0]
                val outputDirectory = args[1]
                val compilationTarget = "kotlin"

                tempolin.process(
                    templateDirectory, outputDirectory, compilationTarget,
                ) { println(it) }
            } catch (e: Exception) {
                println("Usage: java -jar tempolin.jar templateDirectory outputdirectory")
            }
        }
    }
}