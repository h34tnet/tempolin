package net.h34t.tempolin

import java.io.File
import java.io.IOException
import java.nio.file.Path
import kotlin.io.path.notExists

class TemplateFileCollector {

    /**
     * Walks the directory recursively and gathers files matching the given filter in directories matching the given
     * filter.
     *
     * @return the list of template files to process.
     */
    fun findFilesInDirectory(
        inputDir: Path,
        fileFilter: (File) -> Boolean = { true }
    ): List<Path> {
        if (inputDir.notExists())
            throw IOException("Input directory $inputDir doesn't exist.")

        return inputDir.toFile().walk()
            .filter { it.isFile && fileFilter(it) }
            .map { inputDir.relativize(it.toPath()) }
            .toList()
    }
}