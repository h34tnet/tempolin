plugins {
    kotlin("jvm") version "1.9.21"
    `java-gradle-plugin`
    id("com.gradle.plugin-publish") version "1.2.1"
}

group = "net.h34t"
version = "1.0.2"

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    implementation(gradleApi())
    implementation("com.squareup:kotlinpoet:1.15.2")

    testImplementation(kotlin("test"))
    testImplementation("com.github.tschuchortdev:kotlin-compile-testing:1.5.0")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(11)
}

java {
    withSourcesJar()
    withJavadocJar()
}

@Suppress("UnstableApiUsage")
gradlePlugin {
    website.set("https://codeberg.org/h34tnet/tempolin")
    vcsUrl.set("https://codeberg.org/h34tnet/tempolin.git")

    plugins {
        create("tempolin") {
            id = "net.h34t.tempolin"
            displayName = "Tempolin Template Engine"
            description = "A templating engine for kotlin that creates code from text templates."
            implementationClass = "net.h34t.tempolin.TempolinPlugin"

            tags.set(listOf("template", "template engine", "kotlin", "code", "html", "generator"))
        }
    }
}
