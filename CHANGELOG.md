# Changelog

## 1.0.0

* Adds config option for specifying the template interfaces name and whether to actually create the file.

## 0.9.4

* Adds whitespace removal if `for` or `if` constructs are the only non-whitespace tokens on a line.

## 0.9.3

* Fixes misnaming of several control structs: `{skip}`, `{comment}` and `{literal}`. They're now called `literal` and
  `comment`, `skip` is removed.

## 0.9.2

* Changes plugin configuration style; now also supports multiple sets of config options.
* Update to Gradle 8.5.

## 0.9.1

* Initial release